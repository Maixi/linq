﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Folders
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetDirectories();
            //GetFiles();
            //StreamReaderExample();
            //FileStream();
            //Encyprion 
            //Virus
            Console.ReadLine();
        }

        private static void GetDirectories()
        {
            var rootPath = @"D:\education";
            //directory search options , change rootPath to D:\
            var dirs = Directory.GetDirectories(rootPath, "*", SearchOption.AllDirectories);
            dirs.ForEach(Console.WriteLine);
            //folder add 
            var createDir = @"D:\education\dir";
            var directory = @"D:\education\";
            var newDirName = @"dir";
            Directory.CreateDirectory(directory+newDirName);
            #region  what to do if it already exists ?
            if (!Directory.Exists(createDir))
            {
                Directory.CreateDirectory(createDir);
            }
            #endregion

            //second param , what it means ? 
            Directory.Delete(createDir,true);

            #region some fun

            var createDirForFun = @"D:\education\dir";

            for (int i = 0; i < 100; i++)
            {
                Directory.CreateDirectory(createDirForFun);
                createDirForFun = createDirForFun + @"\hehe";
            }
            var dir = Directory.GetDirectories(@"D:\education\dir", "*", SearchOption.AllDirectories);
            dir.ForEach(Console.WriteLine);
            #endregion

            #region DirectoryInfo
            #region Difference
            //Directory is a static class that provides static methods for working with directories.DirectoryInfo is an instance of a class that provides information about a specific directory.
            #endregion
            


            var dirInfo = new DirectoryInfo(@"D:\education");
            if (dirInfo.Exists)
            {
                var files = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);
                files.ForEach(Console.WriteLine);
            }
            #endregion
    }

        private static void GetFiles()
        {
            //again , we have File and FileInfo
            var dir = @"D:\education\";
            var fileName = "test.txt";
            var fullSource = dir + fileName;
            if (!File.Exists(fullSource))
            {
                File.Create(fullSource);
            }
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //File.Delete(fullSource);

            #region some fun
            var filePath = @"D:\education\newFolder";
            var dirInfo = new DirectoryInfo(filePath);
            dirInfo.Create();
            var rnd = new Random();

            for (var i = 0; i < 10; i++)
            {
                var name = string.Empty;
                if (i % 2 == 0)
                {
                    name = GetLetter(rnd) + GetLetter(rnd) + GetLetter(rnd) + ".txt";
                }
                else
                {
                    name = Guid.NewGuid().ToString() + ".txt";
                }
                File.Create(filePath + $@"\{name}");
            }

            var allFiles = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);
            allFiles.OrderBy(x => x.Name).ForEach(Console.WriteLine);

            static string GetLetter(Random rnd)
            {
                char[] randomChars = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
                int num = rnd.Next(0, randomChars.Length);
                return randomChars[num].ToString();
            }
            #endregion

        }

        private static void StreamReaderExample()
        {

            // epub mobi fb2 csv ?
            // epub -> mobi 
            // 
            var fileName = @"D:\education\streamReaderExample.txt";
            using (var sr = new StreamReader(fileName, Encoding.UTF8))
            {
                string line;
                while ((line = sr.ReadLine())!=null)
                {
                    Console.WriteLine(line);
                }
            }

            string[] someRandomLines = {"","tylko", "jedno", "w", "głowe", "mam"};

            using (var sr = new StreamWriter(fileName,true))
            {
                foreach (var someRandomLine in someRandomLines)
                { 
                    sr.WriteLine(someRandomLine);
                }
            }

            var qwe = "asd";
        }

        private static void FileStream()
        {
            //difference -> stream//string reader read//write strings 
            //filestream - alpha . he read bytes.
            //memoryStream

            var source = @"D:\education\MOO.png";
            var result =new byte[]{};
            using (var fs = new FileStream(source,FileMode.Open))
            {
                var img = new byte[100000000];
                fs.Read(img,0,img.Length);
                //1GB size 
                fs.Close();
                //here store
                //rename it 
                result = img;
            }

            var newFilePath = @"D:\education\HappyMooDir";
            Directory.CreateDirectory(newFilePath);
            var newFileName = "HappyMoo.png";
            using (var fs = new FileStream(newFilePath+ @"\"+newFileName,FileMode.Create))
            {
                fs.Write(result, 0, result.Length);
            }
        }
    }
}
