﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace EF_Example
{
    class Program
    {
       static void Main(string[] args)
        {
            AddStudent();
            GetAllStudent();
        }

       private static void AddStudent()
       {
           using (var ctx = new SchoolContext())
           {
               var stud = new Student() { StudentName = "Bill" , DateOfBirth = DateTime.UtcNow, Height = 2, Weight = 90};

               ctx.Students.Add(stud);
               ctx.SaveChanges();
           }
        }

       private static void GetAllStudent()
       {
           using (var context = new SchoolContext())
           {
              var students =  context.Students.ToList();
              students.ForEach(x=> Console.WriteLine(x.StudentName + " " + x.Weight.ToString()));
           }
       }
    }
}
