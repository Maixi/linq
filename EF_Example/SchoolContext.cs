﻿
using Microsoft.EntityFrameworkCore;

namespace EF_Example
{
    public class SchoolContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Courses { get; set; }
       
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source =localhost; Initial Catalog = EducationEF; Integrated Security=true;");
        }
    }
}
