﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{


    class  Human
    {
        public virtual void Say()
        {
            Console.WriteLine("Hello world");
        }
    }

    class Boozer : Human
    {
        public override void Say()
        {
            Console.WriteLine("WorldHello");
        }
    }

    class Language_Integrated_Query
    {
        #region Data init
        private static readonly List<Data> _data = new List<Data>()
        {
            {new Data {Id = 0, Name = "The"}},
            {new Data {Id = 1, Name = "Answer"}},
            {new Data {Id = 2, Name = "For"}},
            {new Data {Id = 3, Name = "The"}},
            {new Data {Id = 4, Name = "Main"}},
            {new Data {Id = 5, Name = "Question"}}
        };

        private static readonly List<StringColor> _colors = new List<StringColor>()
        {
            {new StringColor {DataId = 0, ColorId = ConsoleColor.Yellow}},
            {new StringColor {DataId = 1, ColorId = ConsoleColor.Cyan}},
            {new StringColor {DataId = 2, ColorId = ConsoleColor.DarkRed}},
            {new StringColor {DataId = 3, ColorId = ConsoleColor.Yellow}},
            {new StringColor {DataId = 4, ColorId = ConsoleColor.Cyan}},
            {new StringColor {DataId = 5, ColorId = ConsoleColor.Magenta}},
            {new StringColor {DataId = 42, ColorId = ConsoleColor.White}},
        };

        #endregion

        static void Main(string[] args)
        {

            Boozer boozer = new Boozer();
            
            boozer.Say();

            Human human = boozer;

            human.Say();
            //SelectExample();
            //OrderByExample();
            //TakeExample();
            //JoinExample();
            //GroupingExample();
            //FirstOrDefaultExample();
            //UnionExample();
            //TakeWhile();
            Console.ReadLine();
        }


        private static void TakeWhile()
        {
            var array = new int[] {5,4,1,3,2}.ToList();
         
            var test = array.TakeWhile(x=> x < array.IndexOf(x)).ToList();

            var qwe = test; 
        }

        #region Examples
        //Select
        private static void SelectExample()
        {
            //3 steps for linq
            //obtain data 
            //write query 
            //execute query

            //select of a singe name
            var words = _data.Select(x => x.Id);
            //or like this
            var query = from data in _data
                select data.Id;

          //  var test = _data.Select(x => x.Name);

            
            // we can select only first letters as examp
               //var firstLetters = _data.Select(x => x.Name.Substring(0, 1));
               //var lastLetters = _data.Select(x => new string(x.Name.Reverse().ToArray()).Substring(0, 1));

               var avgValue = _data.Select(x => x.Id).Average();
              Console.WriteLine(avgValue);
              var maxValue = _data.Select(x => x.Id).Max();
              Console.WriteLine(maxValue);
              // words.ForEach(Console.WriteLine);

              //immidiate execution Count, Max, Average, and First // tolist // to array 
        }

        //Ordering
        private static void OrderByExample()
        {
            var words = _data.Select(x=>x.Name).ToArray();
            //like this
            //IEnumerable<string> query = from word in words
            //                            orderby word.Length
            //                            select word;
            //foreach (string str in query)
            //    Console.WriteLine(str);

            //can be ordered by any param ( length, first letter)

            //or like this
            //data 
            //list<String> Names 
            //var result = words.Select(x => x.Name.Substring(0,1)).OrderByDescending(z => z);
            // string value -> A-Z -> int countable 0->infinity 
            // order bydesc => Z-A -> infinity -> 0 
            //
            //result.ForEach(Console.WriteLine);
        }


        private static void TakeExample()
        {
            //take//skip//take last . etc
            var data = _data.Select(x=>x.Id).OrderByDescending(x=>x).Skip(2).Take(1);
            //cant be executed in query language 
            data.ForEach(Console.WriteLine);
        }


        private static void JoinExample()
        {
           

            //data - > Id , Name
            var firstDataSource = _data;
            //color -> DataId, ColorName 
            var secondDataSource = _colors;

            var joinedTables = from x in firstDataSource
                join y in secondDataSource
                    on x.Id equals y.DataId
                select new {y.ColorId, x.Name , x.Id, y.DataId};

            foreach (var res in joinedTables)
            {
                Console.ForegroundColor = res.ColorId;
                Console.WriteLine(res.Name);
            }

            var result = _data.Join(_colors, d => d.Id, c => c.DataId,
                (d, c) => new { ColorType = c.ColorId, DataName = d.Name });

            //var result = from data in _data
            //    join color in _colors on data.Id equals color.DataId
            //    select new { color.ColorId, data.Name };

            //foreach (var res in result)
            //{
            //    Console.ForegroundColor = res.ColorId;
            //    Console.WriteLine(res.Name);
            //}
        }

        private static void GroupingExample()
        {
            //id + name
            var firstData = _data;
            //id + name
            var second = _colors;

            var result = from data in _data
                join color in _colors on data.Id equals color.DataId
                select new { color.ColorId, data.Name, data.Id, color.DataId };
            // colorId , name
            //blue Answer
            //orange THe 
            //etc 


            var colorsGroup = result.GroupBy(x => x.Name);


            foreach (var stringColor in colorsGroup)
            {
                var key = stringColor.Key;
                var data = stringColor.Select(x => x.ColorId).FirstOrDefault();
                Console.ForegroundColor = data;
                Console.WriteLine($"Console Color is {stringColor.Key}");
                var resultString = string.Join(',', stringColor.Select(x => x.Name.ToString()));
                Console.WriteLine(resultString);
            }
        }

        private static void FirstOrDefaultExample()
        {
            var colours = _colors;
            //var idOfFirstGray = colours.First(x => x.ColorId == ConsoleColor.Gray);
            //var idOfFirstYellow = colours.Single(x => x.ColorId == ConsoleColor.Magenta);
            //ToList , ToArray 
            //FirstOrDefault , SingleOrDefault , First , Single 
            //var dict = new Dictionary<int,int>();
            //dict.FirstOrDefault(x => x.Key == 1);
            //var list = new List<int>();
            //list.FirstOrDefault(x => x == 1);
            //first
            //single
            //singleordefault

            //Console.WriteLine(idOfFirstYellow?.DataId);
            //Console.WriteLine(idOfFirstGray?.DataId);

            var whereExmaple = _colors.Where(x => x.ColorId == ConsoleColor.Yellow);

            //single // first OrDe
            //Where -> Limit 1 as 
            //First as for first or default 
            var test = whereExmaple.ToList();

            var res = test.Select(x => x.DataId);
            res.ForEach(Console.WriteLine);
        }


        private static void UnionExample()
        {
            var firstColors = new[] { ConsoleColor.Black,ConsoleColor.DarkBlue,ConsoleColor.DarkRed,ConsoleColor.DarkCyan };
            var secondColors = new[] { ConsoleColor.Yellow,ConsoleColor.DarkMagenta,ConsoleColor.DarkRed,ConsoleColor.Black };

            var union = firstColors.Union(secondColors);
            //union can be by any param , even by objects , you just need to invoke params like this 
            union.Select(x=>x.ToString()).ForEach(Console.WriteLine);

            //come weird code 
            var firstPairs = _data.Select(x => x.Id);
            var secondPair = _colors.Select(x => x.DataId);
            // [0,1,2,3,4,5]
            // [0,1,2,3,4,5,42]

            var test = secondPair.Except(firstPairs);
            foreach (var i in test)
            {
                Console.WriteLine($"answer : {i}");
            }
        }

        #endregion


        #region Data Structure
        private class Data
        {
            public int Id { get; set; }
            public string Name { get; set; } 
        }

        public class StringColor
        {
            public int DataId { get; set; }
            public ConsoleColor ColorId { get; set; }
        }
        #endregion

    }
}
