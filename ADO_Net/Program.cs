﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ADO_Net
{
    class ADO_Net
    {
        private static readonly string _connectionString = "Data Source =localhost; Initial Catalog = Education; Integrated Security=true;";
        static void Main(string[] args)
        {
            //TestConnection();
            //CreateTable();
            //InsertData();
            //var products = new List<Product>()
            //{
            //    new Product{Id = Guid.NewGuid(),Name = "Happy Three Friends",Price = 777, CreatedOn = DateTime.UtcNow},
            //    new Product{Id = Guid.NewGuid(),Name = "Witcher",Price = 888, CreatedOn = DateTime.UtcNow},
            //    new Product{Id = Guid.NewGuid(),Name = "CyberPunk",Price = 999, CreatedOn = DateTime.UtcNow},
            //};
            //products.ForEach(x=>InsertProduct(x.Id,x.Name,x.Price,x.CreatedOn));
            //UpdateProduct();
            //DeleteProduct();
            //Select();
        }

        private static void TestConnection()
        {
            using (var connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}",
                    connection.ConnectionString);
                Console.ReadLine();
            }
        }
        private static void CreateTable()
        {
            using (var connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                var query =
                    @"CREATE TABLE dbo.Products
                (
                    ID uniqueidentifier NOT NULL,
                    Name nvarchar(50) NULL,
                    Price bigint NULL,
                    CreatedOn datetime NULL,
                    CONSTRAINT ProductId PRIMARY KEY (ID)
                );";
                var command = new SqlCommand(query, connection);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    Console.WriteLine("Table Created Successfully");
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Error Generated. Details: " + e.ToString());
                }
                finally
                {
                    connection.Close();
                    Console.ReadKey();
                }
            }
        }

        private static void InsertData()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                //small talk about sql injection and how to pass parametrs correctly
                var query = "INSERT INTO dbo.Products(ID, Name, Price, CreatedOn) VALUES(@ID, @Name, @Price, @CreatedOn)";
                var command = new SqlCommand(query,connection);
                command.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();
                command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "DOOM";
                command.Parameters.Add("@Price", SqlDbType.BigInt).Value = 666;
                command.Parameters.Add("@CreatedOn", SqlDbType.DateTime2).Value = DateTime.UtcNow;
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        private static void InsertProduct(Guid id,string name,long price,DateTime createdOn)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var query = "INSERT INTO dbo.Products(ID, Name, Price, CreatedOn) VALUES(@ID, @Name, @Price, @CreatedOn)";
                var command = new SqlCommand(query, connection);
                command.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Value = id;
                command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = name;
                command.Parameters.Add("@Price", SqlDbType.BigInt).Value = price;
                command.Parameters.Add("@CreatedOn", SqlDbType.DateTime2).Value = createdOn;
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        private static void UpdateProduct()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var query = "Update dbo.Products set Price = @Price where Name = @Name";
                var cmd = new SqlCommand(query,connection);
                cmd.Parameters.Add("@Price", SqlDbType.BigInt).Value = 2222;
                cmd.Parameters.Add(@"Name", SqlDbType.NVarChar).Value = "Witcher";
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private static void DeleteProduct()
        {
            using (var con = new SqlConnection(_connectionString))
            {
                var query = "Delete from dbo.Products where Name = @Name";
                var command = new SqlCommand(query,con);
                command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "DOOM";
                con.Open();
                command.ExecuteNonQuery();
            }
        }

        private static void Select()
        {
            using (var conntection = new SqlConnection(_connectionString))
            {
                string queryString =
                    "SELECT ID, Price, Name from dbo.Products "
                    + "WHERE Price > @Price "
                    + "ORDER BY Price DESC;";
                var command = new SqlCommand(queryString, conntection);
                command.Parameters.Add(@"Price", SqlDbType.BigInt).Value = 766;
                conntection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine("\t{0}\t{1}\t{2}",
                        reader[0], reader[1], reader[2]);
                }
                reader.Close();
            }
        }
        private class Product
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public long Price { get; set; }
            public DateTime CreatedOn { get; set; }
        }
    }
}
